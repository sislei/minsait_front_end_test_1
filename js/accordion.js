var acc = document.getElementsByClassName("accordion");


for (var i = 0; i < acc.length; i++) {
	
  acc[i].addEventListener("click", function() {

  	
    document.getElementById("chevron"+this.id).classList.toggle("chevron--right");
    document.getElementById("chevron"+this.id).classList.toggle("chevron--down"); 
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}